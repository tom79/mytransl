package com.github.stom79.mytransl.test;
/* Copyright 2017 Thomas Schneider
 *
 * This file is a part of MyTransL
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * MyTransL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with MyTransL; if not,
 * see <http://www.gnu.org/licenses>. */

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.github.stom79.mytransl.MyTransL;
import com.github.stom79.mytransl.client.HttpsConnectionException;
import com.github.stom79.mytransl.client.Results;
import com.github.stom79.mytransl.translate.Params;
import com.github.stom79.mytransl.translate.Translate;


public class TranslatorActivity extends AppCompatActivity {

    private TextView targeted, contentObfuscated;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translator);

        TextView content = findViewById(R.id.content_to_translate);
        Button action = findViewById(R.id.translate);
        targeted = findViewById(R.id.content_translated);
        contentObfuscated = findViewById(R.id.content_obfuscated);

        //Initialize MyTransL with an engine translator (currently only Yandex)
        final MyTransL myTransL = MyTransL.getInstance(MyTransL.translatorEngine.LIBRETRANSLATE);
        myTransL.setLibretranslateDomain("translate.fedilab.app");
        //Set the API key
        //    myTransL.setSystranAPIKey("09ba9e59-bac7-4327-9c8e-c2a16f4190a1");
        //    myTransL.setYandexAPIKey("trnsl.1.1.20171128T111334Z.052e85830878b08b.824977799099c2792529b39eba7991e0dff50579");
        //myTransL.setDeeplAPIKey("xxxxxxxxxxxxxxx");
        //Default value is 30 - (in seconds)
        myTransL.setTimeout(60); // 1 minute

        //--- Obfuscation ---
        //Emails, mentions (@name or @name@domain), emails, tags will be obfuscated before being sent to the API
        //Default - false
        myTransL.setObfuscation(true);

        //Extract the content to translate (String)
        final String contentToTranslate = content.getText().toString();

        final Params params = new Params();
        params.setSource_lang("auto");
        //etc. See: https://www.deepl.com/api.html#api_reference_article

        action.setOnClickListener(view -> {
            //Translation is done with the language set in the device via myTransL.getLocale()
            myTransL.translate(contentToTranslate, MyTransL.getLocale(), params, new Results() {
                @Override
                public void onSuccess(Translate translate) {
                    targeted.setText(translate.getTranslatedContent());
                    if (myTransL.isObfuscated()) {
                        contentObfuscated.setText(translate.getObfuscateContent());
                    } else {
                        contentObfuscated.setText(translate.getInitialContent());
                    }
                }

                @Override
                public void onFail(HttpsConnectionException httpsConnectionException) {
                    Toast.makeText(getApplicationContext(), httpsConnectionException.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        });
    }
}
