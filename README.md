

# MyTransL [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)



**MyTransL** is a small library which allows to translate text using [LibreTranslate](https://github.com/uav4geo/LibreTranslate), [Yandex Translate](https://tech.yandex.com/translate/) or the [DeepL API](https://www.deepl.com/api.html)

You need at least to create a free [Yandex translate API key](https://translate.yandex.com/developers/keys) or your [DeepL API key](https://www.deepl.com/pro-registration.html).

PS: for LibreTranslate, you do need to use your own server or ask a [hosting solution](https://uav4geo.com/contact)


<img src="./screenshots/pic_1.png" alt="Example of a translation" width="300" />

## How to use

### Integration


Step 1\. Add the JitPack repository to your build file. Add it in your root build.gradle at the end of repositories:

```java
allprojects {
  repositories {
    ...
    maven { url "https://jitpack.io" }
  }
}
```


Step 2\. Add the dependency - For the correct value of x.y, please refer to the [releases](https://github.com/stom79/mytransl/releases).

```java
dependencies {
        compile 'com.github.stom79:mytransl:x.y'
}
```


This library uses the [Internet permission](https://developer.android.com/reference/android/Manifest.permission.html#INTERNET).

### Usage

Once the project has been added to gradle, you can translate content:

```java
//Initialize MyTransL with the engine translator
MyTransL myTransL = MyTransL.getInstance(MyTransL.translatorEngine.YANDEX);
//Or for DeepL
//MyTransL myTransL = MyTransL.getInstance(MyTransL.translatorEngine.DEEPL);

//Set the API key
myTransL.setYandexAPIKey("YOUR_YANDEX_API_KEY");
//Or for DeepL
//myTransL.setDeeplAPIKey("DEEPL_API_KEY");

//Change the default timeout (30s)
myTransL.setTimeout(60); // 1 minute


//--- Obfuscation ---
//Emails, mentions (@name or @name@domain), emails, tags will be obfuscated before being sent to the API
//Default - false
myTransL.setObfuscation(true);


//Extract the content to translate (String) - "content" is a TextView in this example.
final String contentToTranslate = content.getText().toString();

//Asynchronous call
//Using myTransL.getLocale() will allow to target automatically the language of the device (ie: "en", "fr", etc.)
//This value can be replaced by a fixed locale (ie: "en", "fr", "ru", etc.)
myTransL.translate(contentToTranslate, myTransL.getLocale(), new Results() {
    @Override
    public void onSuccess(Translate translate) {
        String translatedContent = translate.getTranslatedContent();
    }

    @Override
    public void onFail(HttpsConnectionException httpsConnectionException) {
        Toast.makeText(getApplicationContext(), httpsConnectionException.getMessage(), Toast.LENGTH_LONG).show();
    }
});

```

Special parameters with DeepL can be set. See: [api_reference_article](https://www.deepl.com/api.html#api_reference_article):


```java
myTransL.setDeeplAPIKey("DEEPL_API_KEY");

final DeepLParams deepLParams = new DeepLParams();
deepLParams.setSplit_sentences(false);
deepLParams.setSource_lang("fr");
//etc. See: https://www.deepl.com/api.html#api_reference_article

action.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        //Translation is done with the language set in the device via myTransL.getLocale()
        myTransL.translate(contentToTranslate, myTransL.getLocale(), deepLParams, new Results() {
            @Override
            public void onSuccess(Translate translate) {
                targeted.setText(translate.getTranslatedContent());
                mentions.setVisibility(View.VISIBLE);
                if(myTransL.isObfuscated()){
                    contentObfuscated.setText(translate.getObfuscateContent());
                }else{
                    contentObfuscated.setText(translate.getInitialContent());
                }
            }

            @Override
            public void onFail(HttpsConnectionException httpsConnectionException) {
                Toast.makeText(getApplicationContext(), httpsConnectionException.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
});
```

### LibreTranslate

Example of config:

```java
   final MyTransL myTransL = MyTransL.getInstance(MyTransL.translatorEngine.LIBRETRANSLATE);
   myTransL.setLibretranslateDomain("example.com");
   myTransL.setTimeout(60); // 1 minute
   myTransL.setObfuscation(true);
```
PS: for LibreTranslate, you do need to use your own server or ask a [hosting solution](https://uav4geo.com/contact)

### Further Reading

Yandex: [Terms of the Use of the Service](https://yandex.com/legal/translate_api/)

DeepL: [DeepL Pro](https://www.deepl.com/api.html)


Special thanks to [DeepL](https://www.deepl.com/), which offered me a demo version for my tests.